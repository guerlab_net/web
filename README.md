# guerlab-web

guerlab web工具集

## maven仓库地址

```
<dependency>
	<groupId>net.guerlab</groupId>
	<artifactId>guerlab-web</artifactId>
	<version>1.3.0</version>
</dependency>
```

## 更新记录

### 20180426 v1.3.0

- 更新依赖guerlab-commons 1.2.0 -> 1.3.0
- 增加net.guerlab.web.result.ListObject
- 增加net.guerlab.web.result.ReadDataListCommand
- 增加net.guerlab.web.result.ResultUtil