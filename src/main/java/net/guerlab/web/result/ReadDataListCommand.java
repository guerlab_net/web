package net.guerlab.web.result;

import java.util.Collection;

/**
 * 读取数据列表命令
 *
 * @author guer
 *
 * @param <T>
 *            参数类型
 */
@FunctionalInterface
public interface ReadDataListCommand<T> {

    /**
     * 获取数据列表
     *
     * @return 数据列表
     */
    Collection<T> getData();
}
